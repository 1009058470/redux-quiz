import {combineReducers} from "redux";
import mainReducers from "../main/reducers/mainReducers";

const reducers = combineReducers({
  main: mainReducers
});
export default reducers;