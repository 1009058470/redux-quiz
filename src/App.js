import React, {Component} from 'react';
import './App.less';
import {Route, Switch} from "react-router";
import { BrowserRouter as Router } from "react-router-dom";
import main from "./main/pages/main";
import details from "./details/pages/details";
import create from "./create/pages/create";

class App extends Component{
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={main}/>
          <Route path = '/notes/create' component={create} />
          <Route path='/notes/:id' component={details} />
        </Switch>
      </Router>

    );
  }
}

export default App;