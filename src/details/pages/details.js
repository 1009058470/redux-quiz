import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import {bindActionCreators} from "redux";
import {getMainData} from "../../main/action/mainAction";
import {connect} from "react-redux";
import {main} from "../../main/pages/main";

export class details extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      description:'',
      curId: 0
    };

  }

  componentDidMount() {
    this.props.getMainData();
    const id =  this.props.match.params.id; //获取当前页面的id

    const result = this.props.mainDetails;
    if(result[id-1] !== undefined){
      this.setState({
        description: result[id-1].description,
        curId :id
      });
    }

  }


  deleteNotes(){

    this.setState({
      curId:this.props.match.params.id,
    }, () => {
      console.log(this.state.curId);
      const URL = "http://localhost:8080/api/posts/"+this.state.curId;   // 不能直接在fetch 写 /api/posts  它会把当前端口号给你
      fetch(URL ,{method: 'delete',})
        .then(response => response.json())
        .then(result=>console.log(result));
    });



  }

  render() { /* 首页链接可以进入 不过页面内链接无法进入  */
    return (
      <div>
        <section className='left-link'>
          <NavLink exact to='/notes/1'> C# 和 .NET Framework 简介 </NavLink>
          <NavLink exact to='/notes/2'> 编译 </NavLink>
          <NavLink exact to='/notes/3'> 方法 </NavLink>
        </section>


        <section className='right'>
          <p>
            {this.state.description}
          </p>
        </section>

        <footer>
          <button onClick={this.deleteNotes.bind(this)}>删除</button>

          <NavLink exact to='/'>
            <button>返回</button>
          </NavLink>

        </footer>

      </div>
    );
  }

}


const mapStateToProps = state => ({
  mainDetails: state.main.mainDetails
});  // 从redux 的 state 拿数据

const mapDispatchToProps = dispatch => bindActionCreators({
  getMainData
}, dispatch); // 给redux 设置数据

export default connect(mapStateToProps, mapDispatchToProps)(details);