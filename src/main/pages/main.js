import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getMainData} from "../action/mainAction";
import {NavLink} from "react-router-dom";



export class main extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      numOfBooks: 0
    }
  }

  componentDidMount() {
    this.props.getMainData();
  }

  render() {
    const result = this.props.mainDetails;

    const title = [];   //首页的数据title
    for(let i = 0; i < result.length; i++){
      title.push(result[i].title);
    }

    const numOfBook = title.length;



    return (
      <div>
        <header>
          <p>NOTES</p>
        </header>

        <section>

          <nav>
            <NavLink exact to='/notes/1'>
              <button> C# 和 .NET Framework 简介 </button>
            </NavLink>

            <NavLink exact to='/notes/2' activeClassName='active'>
              <button>  编译 </button>
            </NavLink>

            <NavLink exact to='/notes/3' activeClassName='active'>
              <button> 方法  </button>
            </NavLink>
          </nav>

        </section>

      </div>
    );
  }

}


const mapStateToProps = state => ({
  mainDetails: state.main.mainDetails
});  // 从redux 的 state 拿数据

const mapDispatchToProps = dispatch => bindActionCreators({
  getMainData
}, dispatch); // 给redux 设置数据

export default connect(mapStateToProps, mapDispatchToProps)(main);
