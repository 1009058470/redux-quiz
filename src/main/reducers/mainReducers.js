const initState = {
  mainDetails: {}
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'SET_MAIN_DATA':  // 去处理main 的数据
      return {
        ...state,
        mainDetails: action.mainDetails
      };

    default:
      return state
  }
};
