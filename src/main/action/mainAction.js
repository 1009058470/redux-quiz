export const getMainData = () => (dispatch) => {
  fetch("http://localhost:8080/api/posts")
    .then(response => response.json())
    .then(result => {
      dispatch({
        type: 'SET_MAIN_DATA',
        mainDetails: result
      });
    })
};
